#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include "md5.h"

int main( int argc, char *argv[] )
{
    if( argc < 3 )
    {
        printf( "You must include two file names." );
        exit(1);
    }
    
    FILE *source = fopen( argv[1], "r" );
    if( !source )
    {
        perror( "Can't open the source file" );
    }
    
    FILE *destination = fopen( argv[2], "w" );
    if( !destination )
    {
        perror( "Can't open the destination file" );
    }
    
    char line[256];
    
    while( fgets(line, sizeof(line), source) != NULL )
    {
        char *e = line;
        while (*e && *e != '\r' && *e != '\n')
            e++;
        char *hash = md5( line, e - line);
        fprintf( destination, "%s", hash );
        fprintf( destination, " %s", line );
        free( hash );
    }
    

}